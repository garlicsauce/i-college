package io.garlicsauce.icollege.group

import io.garlicsauce.icollege.student.Student
import io.garlicsauce.icollege.teacher.Teacher
import org.apache.commons.lang3.RandomStringUtils
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.util.NestedServletException
import spock.lang.Specification
import org.hamcrest.Matchers

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath

class GroupSpec extends Specification implements GroupsModuleTrait {

    def mockMvc = MockMvcBuilders.standaloneSetup(groupsController).build()

    def "should be able to create an group"() {
        given:
        def group = group()

        when:
        def call = mockMvc.perform(post('/groups')
                .content(groupCreationRequest(group))
                .contentType(MediaType.APPLICATION_JSON))

        then:
        call.andExpect(status().isCreated())

        and:
        groupRepository.findAll().any {
            it.symbol == group.getSymbol()
        }
    }

    def "should return all groups when calling find all groups"() {
        given:
        def group = groupRepository.save(group())

        when:
        def call = mockMvc.perform(get('/groups')
                .contentType(MediaType.APPLICATION_JSON))

        then:
        call.andExpect(status().isOk())

        and:
        call.andExpect(jsonPath('$', Matchers.hasSize(1)))
            .andExpect(jsonPath('$[0].id', Matchers.is(group.getId().toString())))
            .andExpect(jsonPath('$[0].symbol', Matchers.is(group.getSymbol())))
            .andExpect(jsonPath('$[0].teacher', Matchers.is(group.getTeacher())))
            .andExpect(jsonPath('$[0].members', Matchers.is(group.getMembers())))
            .andExpect(jsonPath('$[0].schedule', Matchers.is(group.getSchedule())))
    }

    def "should return empty collection when calling find all groups on empty db"() {
        when:
        def call = mockMvc.perform(get('/groups')
                .contentType(MediaType.APPLICATION_JSON))

        then:
        call.andExpect(status().isOk())

        and:
        call.andExpect(jsonPath('$', Matchers.hasSize(0)))
    }

    def "should return specific group when calling find group by id"() {
        given:
        def group = groupRepository.save(group())
        def initialGroupId = group.getId()

        when:
        def call = mockMvc.perform(get("/groups/$initialGroupId")
                .contentType(MediaType.APPLICATION_JSON))

        then:
        call.andExpect(status().isOk())

        and:
        call.andExpect(jsonPath('$.id', Matchers.is(group.getId().toString())))
            .andExpect(jsonPath('$.symbol', Matchers.is(group.getSymbol())))
            .andExpect(jsonPath('$.teacher', Matchers.is(group.getTeacher())))
            .andExpect(jsonPath('$.members', Matchers.is(group.getMembers())))
            .andExpect(jsonPath('$.schedule', Matchers.is(group.getSchedule())))
    }

    def "should return 404 when looking for non existing group id"() {
        given:
        def nonExistingId = UUID.randomUUID().toString()

        when:
        mockMvc.perform(get("/groups/$nonExistingId")
                .contentType(MediaType.APPLICATION_JSON))

        then: // todo change to 404
        def ex = thrown(NestedServletException)
        ex.cause.class == IllegalArgumentException
    }

    def "should be able to assign teacher"() {
        given:
        def group = groupRepository.save(group())
        def initialGroupId = group.getId()

        and:
        def teacher = teacherRepository.save(teacher())
        def request = teacherAssignmentRequest(teacher.getId(), initialGroupId)

        when:
        def call = mockMvc.perform(put("/groups/$initialGroupId/teachers/main")
                .content(request)
                .contentType(MediaType.APPLICATION_JSON))

        then:
        call.andExpect(status().isNoContent())

        and:
        def resultGroup = groupRepository.findById(initialGroupId).get()
        resultGroup.teacher == teacher
    }

    def "should be able to assign students"() {
        given:
        def group = groupRepository.save(group())
        def initialGroupId = group.getId()

        and:
        def student = studentRepository.save(student())
        def request = studentsAssignmentRequest([student.getId()])

        when:
        def call = mockMvc.perform(put("/groups/$initialGroupId/students")
                .content(request)
                .contentType(MediaType.APPLICATION_JSON))

        then:
        call.andExpect(status().isNoContent())

        and:
        def resultGroup = groupRepository.findById(initialGroupId).get()
        resultGroup.members == [student]
    }

    static def group() {
        return Group.builder()
                .id(UUID.randomUUID())
                .symbol(RandomStringUtils.randomAlphabetic(10))
                .members([])
                .build()
    }

    static def groupCreationRequest(Group group) {
        def symbol = group.getSymbol()
        return """
        {
            "symbol": "$symbol"
        }
        """
    }

    static def teacherAssignmentRequest(UUID teacherUuid, UUID groupUuid) {
        def teacherId = teacherUuid.toString()
        def groupId = groupUuid.toString()
        return """
        {
            "teacherId": "$teacherId",
            "groupId": "$groupId"
        }
        """
    }

    static def teacher() {
        return Teacher.builder()
                .id(UUID.randomUUID())
                .firstName(RandomStringUtils.randomAlphabetic(10))
                .lastName(RandomStringUtils.randomAlphabetic(10))
                .email(RandomStringUtils.randomAlphabetic(5) + "@test.com")
                .build()
    }

    static def studentsAssignmentRequest(List<UUID> studentIds) {
        def joinedIds = studentIds.join(",")
        return """
        {
            "studentIds": ["$joinedIds"]
        }
        """
    }

    static def student() {
        def id = UUID.randomUUID()
        def firstName = RandomStringUtils.randomAlphabetic(10)
        def lastName = RandomStringUtils.randomAlphabetic(10)
        def email = RandomStringUtils.randomAlphabetic(5) + "@test.com"
        return Student.builder()
                .id(id)
                .firstName(firstName)
                .lastName(lastName)
                .email(email)
                .build()
    }

}
