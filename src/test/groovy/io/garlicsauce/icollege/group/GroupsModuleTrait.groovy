package io.garlicsauce.icollege.group

import io.garlicsauce.icollege.student.InMemoryStudentRepository
import io.garlicsauce.icollege.student.StudentDataResponseFactory
import io.garlicsauce.icollege.student.StudentFactory
import io.garlicsauce.icollege.student.Students
import io.garlicsauce.icollege.teacher.InMemoryTeacherRepository
import io.garlicsauce.icollege.teacher.TeacherDataResponseFactory
import io.garlicsauce.icollege.teacher.TeacherFactory
import io.garlicsauce.icollege.teacher.Teachers

trait GroupsModuleTrait {

    GroupRepository groupRepository = new InMemoryGroupRepository()
    GroupFactory groupFactory = new GroupFactory()
    InMemoryTeacherRepository teacherRepository = new InMemoryTeacherRepository()
    TeacherFactory teacherFactory = new TeacherFactory()
    Teachers teachers = new Teachers(teacherRepository, teacherFactory) // todo should we use another package classes
    InMemoryStudentRepository studentRepository = new InMemoryStudentRepository()
    StudentFactory studentFactory = new StudentFactory()
    Students students = new Students(studentRepository, studentFactory) // todo should we use another package classes
    Groups groups = new Groups(groupRepository, groupFactory, teachers, students)

    StudentDataResponseFactory studentDataResponseFactory = new StudentDataResponseFactory()
    TeacherDataResponseFactory teacherDataResponseFactory = new TeacherDataResponseFactory()
    ScheduleDataResponseFactory scheduleDataResponseFactory = new ScheduleDataResponseFactory()
    GroupDataResponseFactory groupDataResponseFactory =  new GroupDataResponseFactory(studentDataResponseFactory,
            teacherDataResponseFactory, scheduleDataResponseFactory)

    GroupsController groupsController = new GroupsController(groups, groupDataResponseFactory)
}
