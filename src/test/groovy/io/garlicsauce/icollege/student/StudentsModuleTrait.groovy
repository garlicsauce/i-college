package io.garlicsauce.icollege.student

trait StudentsModuleTrait {

    StudentRepository studentRepository = new InMemoryStudentRepository()
    StudentFactory studentFactory = new StudentFactory()
    Students students = new Students(studentRepository, studentFactory)

    StudentDataResponseFactory studentDataResponseFactory = new StudentDataResponseFactory()

    AvatarStorage avatarStorage = new InMemoryAvatarStorage()
    Avatars avatars = new Avatars(avatarStorage)

    StudentsController studentsController = new StudentsController(students, studentDataResponseFactory, avatars)

}
