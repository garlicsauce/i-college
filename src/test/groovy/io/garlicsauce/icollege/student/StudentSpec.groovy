package io.garlicsauce.icollege.student

import org.apache.commons.lang3.RandomStringUtils
import org.hamcrest.Matchers
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.request.RequestPostProcessor
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.util.NestedServletException
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

// todo more fields than required
class StudentSpec extends Specification implements StudentsModuleTrait {

    def mockMvc = MockMvcBuilders.standaloneSetup(studentsController).build()

    def "should create an student"() {
        given:
        def student = student()

        when:
        def call = mockMvc.perform(post('/students')
                .content(studentCreationRequest(student))
                .contentType(MediaType.APPLICATION_JSON))

        then:
        call.andExpect(status().isCreated())

        and:
        studentRepository.findAll().any {
            it.firstName == student.firstName &&
            it.lastName == student.lastName &&
            it.email == student.email
        }
    }

    def "should return all students when calling find all students"() {
        given:
        def student = studentRepository.save(student())

        when:
        def call = mockMvc.perform(get('/students')
                .contentType(MediaType.APPLICATION_JSON))

        then:
        call.andExpect(status().isOk())

        and:
        call.andExpect(jsonPath('$', Matchers.hasSize(1)))
            .andExpect(jsonPath('$[0].id', Matchers.is(student.getId().toString())))
            .andExpect(jsonPath('$[0].firstName', Matchers.is(student.getFirstName())))
            .andExpect(jsonPath('$[0].lastName', Matchers.is(student.getLastName())))
            .andExpect(jsonPath('$[0].email', Matchers.is(student.getEmail())))
    }

    def "should return empty collection when calling find all students on empty db"() {
        when:
        def call = mockMvc.perform(get('/students')
                .contentType(MediaType.APPLICATION_JSON))

        then:
        call.andExpect(status().isOk())

        and:
        call.andExpect(jsonPath('$', Matchers.hasSize(0)))
    }

    def "should return specific student when calling find student by id"() {
        given:
        def student = studentRepository.save(student())
        def initialStudentId = student.getId()

        when:
        def call = mockMvc.perform(get("/students/$initialStudentId")
                .contentType(MediaType.APPLICATION_JSON))

        then:
        call.andExpect(status().isOk())

        and:
        call.andExpect(jsonPath('$.id', Matchers.is(student.getId().toString())))
            .andExpect(jsonPath('$.firstName', Matchers.is(student.getFirstName())))
            .andExpect(jsonPath('$.lastName', Matchers.is(student.getLastName())))
            .andExpect(jsonPath('$.email', Matchers.is(student.getEmail())))
    }

    def "should return 404 when looking for non existing student id"() {
        given:
        def nonExistingId = UUID.randomUUID().toString()

        when:
        mockMvc.perform(get("/students/$nonExistingId")
                .contentType(MediaType.APPLICATION_JSON))

        then: // todo change to 404
        def ex = thrown(NestedServletException)
        ex.cause.class == IllegalArgumentException
    }

    def "should be able to add avatar for student"() {
        given:
        def student = studentRepository.save(student())
        def initialStudentId = student.getId()

        and:
        MockMultipartFile avatar = avatar()

        when:
        def call = mockMvc.perform(multipartPut("/students/$initialStudentId/photos/avatar", avatar)
                .contentType(MediaType.APPLICATION_JSON))

        then:
        call.andExpect(status().isOk())

        and:
        avatarStorage.getAvatarByStudentId(initialStudentId) == avatar.getBytes()
    }


    def "should return specific avatar when calling find avatar by student id"() {
        given:
        def student = studentRepository.save(student())
        def initialStudentId = student.getId()

        and:
        MockMultipartFile avatar = avatar()
        avatarStorage.store(initialStudentId, avatar)

        when:
        def call = mockMvc.perform(get("/students/$initialStudentId/photos/avatar")
                .contentType(MediaType.APPLICATION_JSON))

        then:
        call.andExpect(status().isOk())

        and:
        //todo assertion for content
        call.andExpect(header().string(HttpHeaders.CONTENT_TYPE, "image/png"))
    }

    def "should return 404 when looking for avatar with non existing student id"() {
        given:
        def nonExistingId = UUID.randomUUID().toString()

        when:
        mockMvc.perform(get("/students/$nonExistingId/photos/avatar")
                .contentType(MediaType.APPLICATION_JSON))

        then: // todo change to 404
        def ex = thrown(NestedServletException)
        ex.cause.message == "Byte array must not be null"
    }

    static def avatar() {
        //do we want test real png ?
        return new MockMultipartFile("avatar", "avatar.text", "text/plain", "some text".getBytes())
    }

    static def student() {
        def id = UUID.randomUUID()
        def firstName = RandomStringUtils.randomAlphabetic(10)
        def lastName = RandomStringUtils.randomAlphabetic(10)
        def email = RandomStringUtils.randomAlphabetic(5) + "@test.com"
        return Student.builder()
                .id(id)
                .firstName(firstName)
                .lastName(lastName)
                .email(email)
                .build()
    }

    static def studentCreationRequest(Student student) {
        def firstName = student.getFirstName()
        def lastName = student.getLastName()
        def email = student.getEmail()
        return """
        {
            "firstName": "$firstName",
            "lastName": "$lastName",
            "email": "$email"
        }
        """
    }

    static MockHttpServletRequestBuilder multipartPut(String path, MockMultipartFile file) {
        return multipart(path)
                .file(file)
                .with(new RequestPostProcessor() {
                    @Override
                    MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
                        request.setMethod("PUT")
                        return request
                    }
                })
    }

}
