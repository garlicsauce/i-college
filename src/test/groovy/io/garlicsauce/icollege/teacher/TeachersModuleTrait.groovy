package io.garlicsauce.icollege.teacher

trait TeachersModuleTrait {

    TeacherRepository teacherRepository = new InMemoryTeacherRepository()
    TeacherFactory teacherFactory = new TeacherFactory()
    Teachers teachers = new Teachers(teacherRepository, teacherFactory)

    TeacherDataResponseFactory teacherDataResponseFactory = new TeacherDataResponseFactory()

    TeachersController teachersController = new TeachersController(teachers, teacherDataResponseFactory)
}
