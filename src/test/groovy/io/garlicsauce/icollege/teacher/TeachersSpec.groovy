package io.garlicsauce.icollege.teacher

import org.hamcrest.Matchers
import org.apache.commons.lang3.RandomStringUtils
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.util.NestedServletException
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath

class TeachersSpec extends Specification implements TeachersModuleTrait {

    def mockMvc = MockMvcBuilders.standaloneSetup(teachersController).build()

    def "should create a teacher"() {
        given:
        def teacher = teacher()

        when:
        def call = mockMvc.perform(post('/teachers')
                .content(teacherCreationRequest(teacher))
                .contentType(MediaType.APPLICATION_JSON))

        then:
        call.andExpect(status().isCreated())

        and:
        teacherRepository.findAll().any {
            it.firstName == teacher.firstName &&
            it.lastName == teacher.lastName &&
            it.email == teacher.email
        }
    }

    def "should return all teachers when calling find all teachers"() {
        given:
        def teacher = teacherRepository.save(teacher())

        when:
        def call = mockMvc.perform(get('/teachers')
                .contentType(MediaType.APPLICATION_JSON))

        then:
        call.andExpect(status().isOk())

        and:
        call.andExpect(jsonPath('$', Matchers.hasSize(1)))
            .andExpect(jsonPath('$[0].id', Matchers.is(teacher.getId().toString())))
            .andExpect(jsonPath('$[0].firstName', Matchers.is(teacher.getFirstName())))
            .andExpect(jsonPath('$[0].lastName', Matchers.is(teacher.getLastName())))
            .andExpect(jsonPath('$[0].email', Matchers.is(teacher.getEmail())))
    }

    def "should return empty collection when calling find all teachers on empty db"() {
        when:
        def call = mockMvc.perform(get('/teachers')
                .contentType(MediaType.APPLICATION_JSON))

        then:
        call.andExpect(status().isOk())

        and:
        call.andExpect(jsonPath('$', Matchers.hasSize(0)))
    }

    def "should return specific teacher when calling find by teacher id"() {
        given:
        def teacher = teacherRepository.save(teacher())
        def teacherId = teacher.getId().toString()

        when:
        def call = mockMvc.perform(get("/teachers/$teacherId")
                .contentType(MediaType.APPLICATION_JSON))

        then:
        call.andExpect(status().isOk())

        and:
        call.andExpect(jsonPath('$.id', Matchers.is(teacher.getId().toString())))
            .andExpect(jsonPath('$.firstName', Matchers.is(teacher.getFirstName())))
            .andExpect(jsonPath('$.lastName', Matchers.is(teacher.getLastName())))
            .andExpect(jsonPath('$.email', Matchers.is(teacher.getEmail())))
    }

    def "should return 404 when looking for non existing teacher id"() {
        given:
        def nonExistingId = UUID.randomUUID().toString()

        when:
        mockMvc.perform(get("/teachers/$nonExistingId")
                .contentType(MediaType.APPLICATION_JSON))

        then: // todo change to 404
        def ex = thrown(NestedServletException)
        ex.cause.class == IllegalArgumentException
    }

    static def teacher() {
        return Teacher.builder()
                .id(UUID.randomUUID())
                .firstName(RandomStringUtils.randomAlphabetic(10))
                .lastName(RandomStringUtils.randomAlphabetic(10))
                .email(RandomStringUtils.randomAlphabetic(5) + "@test.com")
                .build()
    }

    static def teacherCreationRequest(Teacher teacher) {
        def firstName = teacher.getFirstName()
        def lastName = teacher.getLastName()
        def emailName = teacher.getEmail()
        return """
        {
            "firstName": "$firstName",
            "lastName": "$lastName",
            "email": "$emailName"
        }
        """
    }

}
