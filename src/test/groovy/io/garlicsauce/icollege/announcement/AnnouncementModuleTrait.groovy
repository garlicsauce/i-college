package io.garlicsauce.icollege.announcement

trait AnnouncementModuleTrait {

    AnnouncementRepository announcementRepository = new InMemoryAnnouncementRepository()
    AnnouncementFactory announcementFactory = new AnnouncementFactory()
    Announcements announcements = new Announcements(announcementRepository, announcementFactory)

    AnnouncementDataResponseFactory announcementDataResponseFactory = new AnnouncementDataResponseFactory()

    AnnouncementsController announcementsController =
            new AnnouncementsController(announcements, announcementDataResponseFactory)
}
