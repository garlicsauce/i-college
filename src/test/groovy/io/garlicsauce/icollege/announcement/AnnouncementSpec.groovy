package io.garlicsauce.icollege.announcement

import org.apache.commons.lang3.RandomStringUtils
import org.hamcrest.Matchers
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class AnnouncementSpec extends Specification implements AnnouncementModuleTrait {

    def mockMvc = MockMvcBuilders.standaloneSetup(announcementsController).build()

    def "should create an announcement"() {
        given:
        def announcement = announcement()

        when:
        def call = mockMvc.perform(post('/announcements')
                .content(announcementCreationRequest(announcement))
                .contentType(MediaType.APPLICATION_JSON))

        then:
        call.andExpect(status().isCreated())

        and:
        announcementRepository.findAll().any {
            it.title == announcement.title &&
            it.text == announcement.text
        }
    }

    def "should return all announcements when calling find all announcements"() {
        given:
        def announcement = announcementRepository.save(announcement())

        when:
        def call = mockMvc.perform(get("/announcements")
                .contentType(MediaType.APPLICATION_JSON))

        then:
        call.andExpect(status().isOk())

        and:
        mockMvc.perform(get('/announcements'))
                .andExpect(jsonPath('$[0].id', Matchers.not(Matchers.empty())))
                .andExpect(jsonPath('$[0].title', Matchers.is(announcement.getTitle())))
                .andExpect(jsonPath('$[0].text', Matchers.is(announcement.getText())))
    }

    def "should return empty collection when calling find all announcements on empty db"() {
        when:
        def call = mockMvc.perform(get('/announcements')
                .contentType(MediaType.APPLICATION_JSON))

        then:
        call.andExpect(status().isOk())

        and:
        call.andExpect(jsonPath('$', Matchers.hasSize(0)))
    }

    static def announcement() {
        return Announcement.builder()
                .id(UUID.randomUUID())
                .title(RandomStringUtils.randomAlphabetic(10))
                .text(RandomStringUtils.randomAlphabetic(10))
                .build()
    }

    static def announcementCreationRequest(Announcement announcement) {
        def announcementText = announcement.getText()
        def announcementTitle = announcement.getTitle()
        return """
        {
            "title": "${announcementTitle}",
            "text": "${announcementText}"
        }
        """
    }

}
