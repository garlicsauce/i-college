package io.garlicsauce.icollege;

import io.garlicsauce.icollege.group.Group;
import io.garlicsauce.icollege.group.InMemoryGroupRepository;
import io.garlicsauce.icollege.student.ContactData;
import io.garlicsauce.icollege.student.InMemoryStudentRepository;
import io.garlicsauce.icollege.student.Passport;
import io.garlicsauce.icollege.student.Student;
import io.garlicsauce.icollege.student.Visa;
import io.garlicsauce.icollege.teacher.InMemoryTeacherRepository;
import io.garlicsauce.icollege.teacher.Teacher;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;

import java.util.UUID;

@AllArgsConstructor
public class ICollegeApplicationInitialData implements CommandLineRunner {

	private final InMemoryTeacherRepository inMemoryTeacherRepository;
	private final InMemoryStudentRepository inMemoryStudentRepository;
	private final InMemoryGroupRepository inMemoryGroupRepository;

	@Override
	public void run(String... args) {
		inMemoryTeacherRepository.save(sampleTeacher());
		inMemoryStudentRepository.save(sampleStudent());
		inMemoryGroupRepository.save(sampleGroup());
	}

	private static Group sampleGroup() {
		return Group.builder()
				.id(UUID.fromString("a30c4c4f-db5c-4d9f-82eb-1b628d149c8e"))
				.symbol("Gryffindor")
				.build();
	}

	private static Teacher sampleTeacher() {
		return Teacher.builder()
				.id(UUID.fromString("af9439a0-6447-4fc4-b3b5-201dd68e8467"))
				.firstName("Minerva")
				.lastName("McGonagall")
				.email("minerva@mcgonagall")
				.build();
	}

	private static Student sampleStudent() {
		return Student.builder()
				.id(UUID.fromString("f30a536b-9749-4f28-8eda-cd2f1d7819e2"))
				.firstName("Harry")
				.lastName("Potter")
				.email("harry@potter")
				.contactData(ContactData.builder().build())
				.visa(Visa.builder().build())
				.passport(Passport.builder().build())
				.build();
	}
}
