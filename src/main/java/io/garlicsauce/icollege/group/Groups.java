package io.garlicsauce.icollege.group;

import io.garlicsauce.icollege.student.SetStudentsRequest;
import io.garlicsauce.icollege.student.Student;
import io.garlicsauce.icollege.student.Students;
import io.garlicsauce.icollege.teacher.Teacher;
import io.garlicsauce.icollege.teacher.Teachers;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class Groups {

    private final GroupRepository groupRepository;
    private final GroupFactory groupFactory;
    private final Teachers teachers;
    private final Students students;

    public void create(GroupCreationRequest creationRequest) {
        log.info("Creating a group with symbol {}", creationRequest.getSymbol());

        Group group = groupRepository.save(groupFactory.create(creationRequest));
        log.debug("Created a group: {}", group);
    }

    public Group getGroup(UUID groupId) {
        return groupRepository.findById(groupId)
            .orElseThrow(IllegalArgumentException::new); // todo replace with custom exception mapped to 404
    }

    public Collection<Group> getAllGroups() {
        return groupRepository.findAll();
    }

    public void assignTeacher(AssignTeacherRequest assignTeacherRequest) {
        log.info("Assigning teacher to a group");

        Teacher teacher = teachers.getTeacher(assignTeacherRequest.getTeacherId());
        Group group = getGroup(assignTeacherRequest.getGroupId());

        groupRepository.save(group.assignTeacher(teacher));
    }

    public void setStudents(UUID groupId, SetStudentsRequest setStudentsRequest) {
        log.info("Setting students of a group with id {}", groupId);

        Group group = getGroup(groupId);
        List<Student> studentList = students.getStudents(setStudentsRequest.getStudentIds());
        groupRepository.save(group.setStudents(studentList));
    }
}
