package io.garlicsauce.icollege.group;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
class GroupFactory {

    Group create(GroupCreationRequest creationRequest) {
        return Group.builder()
            .id(UUID.randomUUID())
            .symbol(creationRequest.getSymbol())
            .build();
    }
}
