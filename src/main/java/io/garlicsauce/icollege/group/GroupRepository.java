package io.garlicsauce.icollege.group;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

interface GroupRepository {

    Group save(Group group);
    Optional<Group> findById(UUID id);
    Collection<Group> findAll();

}
