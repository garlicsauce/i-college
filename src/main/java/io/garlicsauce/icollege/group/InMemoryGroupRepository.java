package io.garlicsauce.icollege.group;

import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class InMemoryGroupRepository implements GroupRepository {

    private final Map<UUID, Group> repository;

    InMemoryGroupRepository() {
        this.repository = new ConcurrentHashMap<>();
    }

    @Override
    public synchronized Group save(Group group) {
        repository.put(group.getId(), group);
        return group;
    }

    @Override
    public synchronized Optional<Group> findById(UUID id) {
        return Optional.ofNullable(repository.get(id));
    }

    @Override
    public synchronized Collection<Group> findAll() {
        return repository.values();
    }

}
