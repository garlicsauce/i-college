package io.garlicsauce.icollege.group;

import io.garlicsauce.icollege.student.Student;
import io.garlicsauce.icollege.teacher.Teacher;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
@Builder(toBuilder = true)
public class Group {

    private final UUID id;
    private final String symbol;
    private final Teacher teacher;
    private final List<Student> members;
    private final Schedule schedule;

    Group assignTeacher(Teacher teacher) {
        return this.toBuilder()
            .teacher(teacher)
            .build();
    }

    Group setStudents(List<Student> students) {
        return this.toBuilder()
            .members(students)
            .build();
    }
}
