package io.garlicsauce.icollege.group;

import org.springframework.stereotype.Component;

@Component
class ScheduleDataResponseFactory {

    ScheduleDataResponse create(Schedule schedule) {
        if (schedule == null) {
            return null;
        }

        return ScheduleDataResponse.builder()
            .build();
    }
}
