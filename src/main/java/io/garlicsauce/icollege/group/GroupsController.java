package io.garlicsauce.icollege.group;

import io.garlicsauce.icollege.student.SetStudentsRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/groups")
@RequiredArgsConstructor
class GroupsController {

    private final Groups groups;
    private final GroupDataResponseFactory groupDataResponseFactory;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    void addGroup(@RequestBody @Valid GroupCreationRequest creationRequest) {
        log.info("Received request to create a group");

        groups.create(creationRequest);
    }

    @GetMapping("/{id}")
    GroupDataResponse getGroup(@PathVariable("id") UUID groupId) {
        log.info("Received request to find a group with id {}", groupId);

        return groupDataResponseFactory.create(groups.getGroup(groupId));
    }

    @GetMapping
    List<GroupDataResponse> getAllGroups() {
        log.info("Received request to find all group");

        return groupDataResponseFactory.createAll(groups.getAllGroups());
    }

    @PutMapping("/{id}/teachers/main")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void assignTeacher(@PathVariable("id") UUID groupId,
                       @RequestBody @Valid AssignTeacherRequest assignTeacherRequest) {
        log.info("Received request to assign a teacher with id {} to group with id {}",
            assignTeacherRequest.getTeacherId(), groupId);

        groups.assignTeacher(assignTeacherRequest);
    }

    @PutMapping("/{id}/students")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void setStudents(@PathVariable("id") UUID groupId,
                     @RequestBody @Valid SetStudentsRequest setStudentsRequest) {
        log.info("Received request to set list of students to group with id {}", groupId);

        groups.setStudents(groupId, setStudentsRequest);
    }

}
