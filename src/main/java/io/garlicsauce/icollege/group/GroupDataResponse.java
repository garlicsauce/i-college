package io.garlicsauce.icollege.group;

import io.garlicsauce.icollege.student.StudentDataResponse;
import io.garlicsauce.icollege.teacher.TeacherDataResponse;
import lombok.Builder;
import lombok.Value;

import java.util.List;
import java.util.UUID;

@Value
@Builder
class GroupDataResponse {

    UUID id;
    String symbol;
    TeacherDataResponse teacher;
    List<StudentDataResponse> members;
    ScheduleDataResponse schedule;
}
