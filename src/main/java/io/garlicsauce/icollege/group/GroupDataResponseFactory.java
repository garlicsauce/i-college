package io.garlicsauce.icollege.group;

import io.garlicsauce.icollege.student.StudentDataResponseFactory;
import io.garlicsauce.icollege.teacher.TeacherDataResponseFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class GroupDataResponseFactory {

    private final StudentDataResponseFactory studentDataResponseFactory;
    private final TeacherDataResponseFactory teacherDataResponseFactory;
    private final ScheduleDataResponseFactory scheduleDataResponseFactory;

    public List<GroupDataResponse> createAll(Collection<Group> groups) {
        return groups.stream()
            .map(this::create)
            .collect(Collectors.toList());
    }

    public GroupDataResponse create(Group group) {
        return GroupDataResponse.builder()
            .id(group.getId())
            .symbol(group.getSymbol())
            .teacher(teacherDataResponseFactory.create(group.getTeacher()))
            .members(studentDataResponseFactory.createAll(group.getMembers()))
            .schedule(scheduleDataResponseFactory.create(group.getSchedule()))
            .build();
    }
}
