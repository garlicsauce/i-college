package io.garlicsauce.icollege.group;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Value
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor
class AssignTeacherRequest {

    @NotNull UUID teacherId;
    @NotNull UUID groupId;
}
