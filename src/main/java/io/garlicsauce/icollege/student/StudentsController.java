package io.garlicsauce.icollege.student;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
class StudentsController {

    private final Students students;
    private final StudentDataResponseFactory studentDataResponseFactory;
    private final Avatars avatars;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    void registerStudent(@RequestBody @Valid StudentRegistrationRequest registrationRequest) {
        log.info("Received request to register a student");

        students.registerStudent(registrationRequest);
    }

    @GetMapping("/{id}")
    StudentDataResponse getStudent(@PathVariable("id") UUID id) {
        log.info("Received request to find a student with id {}", id);

        return studentDataResponseFactory.create(students.getStudent(id));
    }

    @GetMapping
    List<StudentDataResponse> getAllStudents() {
        log.info("Received request to find all students");

        return studentDataResponseFactory.createAll(students.getAllStudents());
    }

    @PutMapping("/{id}/photos/avatar")
    void addAvatar(@PathVariable("id") UUID id, @RequestParam("avatar") MultipartFile avatar) {
        //todo missing implementation of non existing student ?
        avatars.addAvatar(id, avatar);
    }

    @GetMapping("/{id}/photos/avatar")
    ResponseEntity<Resource> getAvatarOfAStudent(@PathVariable("id") UUID studentId) {
        Resource avatar = new ByteArrayResource(avatars.getAvatarOfAStudent(studentId));
        return ResponseEntity
            .ok()
            .header(HttpHeaders.CONTENT_TYPE, "image/png")
            .body(avatar);
    }
}
