package io.garlicsauce.icollege.student;

import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

interface AvatarStorage {

    void store(UUID studentId, MultipartFile avatar);

    byte[] getAvatarByStudentId(UUID studentId);
}
