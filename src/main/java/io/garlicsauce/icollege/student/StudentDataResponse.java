package io.garlicsauce.icollege.student;

import lombok.Builder;
import lombok.Value;

import java.util.UUID;

@Value
@Builder
public class StudentDataResponse {

    UUID id;
    String firstName;
    String lastName;
    String email;
    String countryOfOrigin;
    String visaNumber;
    String passportNumber;
}
