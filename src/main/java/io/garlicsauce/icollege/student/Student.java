package io.garlicsauce.icollege.student;

import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.UUID;

@Data
@Builder
public class Student {

    private final UUID id;
    private final String firstName;
    private final String lastName;
    private final String email;
    private final ZonedDateTime dateOfBirth;
    private final String countryOfOrigin;
    private final ContactData contactData;
    private final Visa visa;
    private final Passport passport;
    private final String englishProficiencyLevel;
}
