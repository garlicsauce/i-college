package io.garlicsauce.icollege.student;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
class Avatars {

    private final AvatarStorage avatarStorage;

    void addAvatar(UUID studentId, MultipartFile avatar) {
        avatarStorage.store(studentId, avatar);
    }

    byte[] getAvatarOfAStudent(UUID studentId) {
        return avatarStorage.getAvatarByStudentId(studentId);
    }
}
