package io.garlicsauce.icollege.student;

import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class InMemoryAvatarStorage implements AvatarStorage {

    private final Map<UUID, byte[]> repository;

    InMemoryAvatarStorage() {
        this.repository = new ConcurrentHashMap<>();
    }

    @Override
    public synchronized void store(UUID studentId, MultipartFile avatar) {
        try {
            repository.put(studentId, avatar.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public synchronized byte[] getAvatarByStudentId(UUID studentId) {
        return repository.get(studentId);
    }
}
