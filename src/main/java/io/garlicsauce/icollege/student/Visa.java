package io.garlicsauce.icollege.student;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Visa {

    private final String number;
}
