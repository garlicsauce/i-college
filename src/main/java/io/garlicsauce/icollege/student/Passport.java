package io.garlicsauce.icollege.student;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Passport {

    private final String number;
}
