package io.garlicsauce.icollege.student;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

interface StudentRepository {

    Student save(Student student);
    Optional<Student> findById(UUID id);
    Collection<Student> findAll();
    Collection<Student> findAll(Collection<UUID> studentIds);

}
