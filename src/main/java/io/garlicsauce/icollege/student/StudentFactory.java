package io.garlicsauce.icollege.student;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class StudentFactory {

    public Student create(StudentRegistrationRequest registrationRequest) {
        return Student.builder()
            .id(UUID.randomUUID())
            .firstName(registrationRequest.getFirstName())
            .lastName(registrationRequest.getLastName())
            .email(registrationRequest.getEmail())
            .dateOfBirth(registrationRequest.getDateOfBirth())
            .countryOfOrigin(registrationRequest.getCountryOfOrigin())
            .visa(Visa.builder().number(registrationRequest.getVisaNumber()).build())
            .passport(Passport.builder().number(registrationRequest.getPassportNumber()).build())
            .englishProficiencyLevel(registrationRequest.getEnglishProficiencyLevel())
            .build();
    }
}
