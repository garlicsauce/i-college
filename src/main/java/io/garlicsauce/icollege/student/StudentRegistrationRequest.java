package io.garlicsauce.icollege.student;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.validation.constraints.NotBlank;
import java.time.ZonedDateTime;

@Value
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor
class StudentRegistrationRequest {

    @NotBlank
    String firstName;
    @NotBlank
    String lastName;
    @NotBlank
    String email;

    ZonedDateTime dateOfBirth;
    String countryOfOrigin;
    String visaNumber;
    String passportNumber;
    String englishProficiencyLevel;
}
