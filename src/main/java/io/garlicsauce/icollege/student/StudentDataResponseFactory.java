package io.garlicsauce.icollege.student;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class StudentDataResponseFactory {

    public List<StudentDataResponse> createAll(Collection<Student> students) {
        if (students == null) {
            return new ArrayList<>();
        }

        return students.stream()
            .map(this::create)
            .collect(Collectors.toList());
    }

    public StudentDataResponse create(Student student) {
        if (student == null) {
            return null;
        }

        return StudentDataResponse.builder()
            .id(student.getId())
            .firstName(student.getFirstName())
            .lastName(student.getLastName())
            .email(student.getEmail())
            .countryOfOrigin(student.getCountryOfOrigin())
            .visaNumber(student.getVisa() != null ? student.getVisa().getNumber() : null)
            .passportNumber(student.getPassport() != null ? student.getPassport().getNumber() : null)
            .build();
    }
}
