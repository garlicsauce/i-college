package io.garlicsauce.icollege.student;

import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Repository
public class InMemoryStudentRepository implements StudentRepository {

    private final Map<UUID, Student> repository;

    public InMemoryStudentRepository() {
        this.repository = new ConcurrentHashMap<>();
    }

    @Override
    public synchronized Student save(Student student) {
        repository.put(student.getId(), student);
        return student;
    }

    @Override
    public synchronized Optional<Student> findById(UUID id) {
        return Optional.ofNullable(repository.get(id));
    }

    @Override
    public synchronized Collection<Student> findAll() {
        return repository.values();
    }

    @Override
    public synchronized Collection<Student> findAll(Collection<UUID> studentIds) {
        return studentIds.stream()
            .map(this::findById)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .collect(Collectors.toList());
    }

}
