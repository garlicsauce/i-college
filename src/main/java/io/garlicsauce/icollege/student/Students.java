package io.garlicsauce.icollege.student;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class Students {

    private final StudentRepository studentRepository;
    private final StudentFactory studentFactory;

    public void registerStudent(StudentRegistrationRequest registrationRequest) {
        log.info("Registering student named {} {}",
            registrationRequest.getFirstName(), registrationRequest.getLastName());

        Student student = studentRepository.save(studentFactory.create(registrationRequest));
        log.debug("Created a student: {}", student);
    }

    public Student getStudent(UUID studentId) {
        return studentRepository.findById(studentId)
            .orElseThrow(IllegalArgumentException::new); // todo replace with custom exception mapped to 404
    }

    public List<Student> getStudents(Collection<UUID> studentIds) {
        return (List)studentRepository.findAll(studentIds);
    }

    public Collection<Student> getAllStudents() {
        return studentRepository.findAll();
    }
}
