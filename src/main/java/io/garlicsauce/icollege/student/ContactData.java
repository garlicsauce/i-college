package io.garlicsauce.icollege.student;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ContactData {

    private final String street;
    private final String house;
    private final String apartment;
    private final String postcode;
    private final String city;
    private final String country;
}
