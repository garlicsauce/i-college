package io.garlicsauce.icollege;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ICollegeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ICollegeApplication.class, args);
	}

}
