package io.garlicsauce.icollege.teacher;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class TeacherFactory {

    public Teacher create(TeacherRegistrationRequest registrationRequest) {
        return Teacher.builder()
            .id(UUID.randomUUID())
            .firstName(registrationRequest.getFirstName())
            .lastName(registrationRequest.getLastName())
            .email(registrationRequest.getEmail())
            .build();
    }
}
