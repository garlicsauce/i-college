package io.garlicsauce.icollege.teacher;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/teachers")
@RequiredArgsConstructor
class TeachersController {

    private final Teachers teachers;
    private final TeacherDataResponseFactory teacherDataResponseFactory;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    void addATeacher(@RequestBody @Valid TeacherRegistrationRequest registrationRequest) {
        log.info("Received request to add a teacher");

        teachers.addTeacher(registrationRequest);
    }

    @GetMapping("/{id}")
    TeacherDataResponse getTeacher(@PathVariable("id") UUID id) {
        log.info("Received request to find a teacher with id {}", id);

        return teacherDataResponseFactory.create(teachers.getTeacher(id));
    }

    @GetMapping
    List<TeacherDataResponse> getAllTeachers() {
        log.info("Received request to find all teachers");

        return teacherDataResponseFactory.createAll(teachers.getAllTeachers());
    }
}
