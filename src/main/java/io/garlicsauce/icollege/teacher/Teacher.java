package io.garlicsauce.icollege.teacher;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class Teacher {

    private final UUID id;
    private final String firstName;
    private final String lastName;
    private final String email;
}
