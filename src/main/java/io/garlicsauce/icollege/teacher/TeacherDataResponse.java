package io.garlicsauce.icollege.teacher;

import lombok.Builder;
import lombok.Value;

import java.util.UUID;

@Value
@Builder
public class TeacherDataResponse {

    UUID id;
    String firstName;
    String lastName;
    String email;
}
