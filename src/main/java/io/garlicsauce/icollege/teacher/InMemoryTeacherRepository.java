package io.garlicsauce.icollege.teacher;

import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class InMemoryTeacherRepository implements TeacherRepository {

    private final Map<UUID, Teacher> repository;

    public InMemoryTeacherRepository() {
        this.repository = new ConcurrentHashMap<>();
    }

    @Override
    public synchronized Teacher save(Teacher teacher) {
        repository.put(teacher.getId(), teacher);
        return teacher;
    }

    @Override
    public synchronized Optional<Teacher> findById(UUID id) {
        return Optional.ofNullable(repository.get(id));
    }

    @Override
    public synchronized Collection<Teacher> findAll() {
        return repository.values();
    }

}
