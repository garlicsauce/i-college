package io.garlicsauce.icollege.teacher;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class Teachers {

    private final TeacherRepository teacherRepository;
    private final TeacherFactory teacherFactory;

    public void addTeacher(TeacherRegistrationRequest registrationRequest) {
        log.info("Adding a teacher named {} {}",
            registrationRequest.getFirstName(), registrationRequest.getLastName());

        Teacher teacher = teacherRepository.save(teacherFactory.create(registrationRequest));
        log.debug("Created a teacher: {}", teacher);
    }

    public Teacher getTeacher(UUID teacherId) {
        return teacherRepository.findById(teacherId)
            .orElseThrow(IllegalArgumentException::new); // todo replace with custom exception mapped to 404
    }

    public Collection<Teacher> getAllTeachers() {
        return teacherRepository.findAll();
    }
}
