package io.garlicsauce.icollege.teacher;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor
class TeacherRegistrationRequest {

    @NotBlank
    String firstName;
    @NotBlank
    String lastName;
    @NotBlank
    String email;
}
