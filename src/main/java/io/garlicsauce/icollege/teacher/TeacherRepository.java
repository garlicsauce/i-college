package io.garlicsauce.icollege.teacher;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

interface TeacherRepository {

    Teacher save(Teacher teacher);

    Optional<Teacher> findById(UUID id);

    Collection<Teacher> findAll();
}
