package io.garlicsauce.icollege.teacher;

import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TeacherDataResponseFactory {

    public List<TeacherDataResponse> createAll(Collection<Teacher> allTeachers) {
        return allTeachers.stream()
            .map(this::create)
            .collect(Collectors.toList());
    }

    public TeacherDataResponse create(Teacher teacher) {
        if (teacher == null) {
            return null;
        }

        return TeacherDataResponse.builder()
            .id(teacher.getId())
            .firstName(teacher.getFirstName())
            .lastName(teacher.getLastName())
            .email(teacher.getEmail())
            .build();
    }
}
