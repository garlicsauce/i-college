package io.garlicsauce.icollege.announcement;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor
class AnnouncementCreationRequest {

    @NotBlank
    String title;
    @NotBlank
    String text;
}
