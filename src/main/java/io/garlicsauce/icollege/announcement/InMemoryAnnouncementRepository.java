package io.garlicsauce.icollege.announcement;

import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Repository
class InMemoryAnnouncementRepository implements AnnouncementRepository {

    private final Map<UUID, Announcement> repository;

    InMemoryAnnouncementRepository() {
        this.repository = new ConcurrentHashMap<>();
    }

    @Override
    public synchronized Announcement save(Announcement announcement) {
        repository.put(announcement.getId(), announcement);
        return announcement;
    }

    @Override
    public synchronized Collection<Announcement> findAll() {
        return repository.values();
    }
}
