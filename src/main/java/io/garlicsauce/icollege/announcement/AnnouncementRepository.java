package io.garlicsauce.icollege.announcement;

import java.util.Collection;

interface AnnouncementRepository {

    Announcement save(Announcement announcement);

    Collection<Announcement> findAll();
}
