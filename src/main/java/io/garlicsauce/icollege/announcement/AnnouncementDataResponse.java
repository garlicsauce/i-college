package io.garlicsauce.icollege.announcement;

import lombok.Builder;
import lombok.Value;

import java.util.UUID;

@Value
@Builder
class AnnouncementDataResponse {

    UUID id;
    String title;
    String text;
}
