package io.garlicsauce.icollege.announcement;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
class Announcement {

    private final UUID id;
    private final String title;
    private final String text;

}
