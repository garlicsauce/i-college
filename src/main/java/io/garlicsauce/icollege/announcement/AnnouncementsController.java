package io.garlicsauce.icollege.announcement;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/announcements")
@RequiredArgsConstructor
class AnnouncementsController {

    private final Announcements announcements;
    private final AnnouncementDataResponseFactory announcementDataResponseFactory;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    void createAnnouncement(@RequestBody @Valid AnnouncementCreationRequest announcementCreationRequest) {
        log.info("Received request to create an announcement");

        announcements.createAnnouncement(announcementCreationRequest);
    }

    @GetMapping
    List<AnnouncementDataResponse> getAnnouncements() {
        log.info("Received request to find all announcements");

        return announcementDataResponseFactory.createAll(announcements.getAllAnnouncements());
    }
}
