package io.garlicsauce.icollege.announcement;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Slf4j
@Service
@RequiredArgsConstructor
class Announcements {

    private final AnnouncementRepository announcementRepository;
    private final AnnouncementFactory announcementFactory;

    void createAnnouncement(AnnouncementCreationRequest announcementCreationRequest) {
        Announcement announcement = announcementRepository.save(
            announcementFactory.create(announcementCreationRequest));
        log.debug("Created an announcement: {}", announcement);
    }

    Collection<Announcement> getAllAnnouncements() {
        return announcementRepository.findAll();
    }
}
