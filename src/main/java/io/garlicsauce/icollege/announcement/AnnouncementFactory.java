package io.garlicsauce.icollege.announcement;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
class AnnouncementFactory {

    Announcement create(AnnouncementCreationRequest announcementCreationRequest) {
        return Announcement.builder()
            .id(UUID.randomUUID())
            .title(announcementCreationRequest.getTitle())
            .text(announcementCreationRequest.getText())
            .build();
    }
}
