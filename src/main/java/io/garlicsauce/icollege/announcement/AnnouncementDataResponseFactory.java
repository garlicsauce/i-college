package io.garlicsauce.icollege.announcement;

import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
class AnnouncementDataResponseFactory {

    List<AnnouncementDataResponse> createAll(Collection<Announcement> announcements) {
        return announcements.stream()
            .map(this::create)
            .collect(Collectors.toList());
    }

    AnnouncementDataResponse create(Announcement announcement) {
        return AnnouncementDataResponse.builder()
            .id(announcement.getId())
            .title(announcement.getTitle())
            .text(announcement.getText())
            .build();
    }
}
